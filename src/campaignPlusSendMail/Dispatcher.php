<?php
/**
 * Created by PhpStorm.
 * User: Kevinm
 * Date: 18/03/2016
 * Time: 10:58
 */

namespace campaignPlusSendMail;


class Dispatcher
{
    protected $url;
    protected $request;
    protected $response;
    protected $options;
    protected $responseHeader;
    protected $requestHeader;

    /**
     * Dispatcher constructor.
     * @param Request $request
     */
    public function __construct(\campaignPlusSendMail\Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }


    public static function DispatcherFactory(\campaignPlusSendMail\Request $request)
    {
        return new Dispatcher($request);
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     * @return Dispatcher
     */
    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @param bool $decode
     * @return mixed
     */
    public function getResponse($decode = true)
    {
        if($decode){
            return json_decode($this->response);
        }
        return $this->response;
    }

    /**
     * @param mixed $response
     * @return Dispatcher
     */
    private function setResponse($response)
    {
        $this->response = $response;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResponseHeader()
    {
        return $this->responseHeader;
    }

    /**
     * @param mixed $responseHeader
     * @return Dispatcher
     */
    public function setResponseHeader($responseHeader)
    {
        $this->responseHeader = $responseHeader;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequestHeader()
    {
        return $this->requestHeader;
    }

    /**
     * @param mixed $requestHeader
     * @return Dispatcher
     */
    public function setRequestHeader($requestHeader)
    {
        $this->requestHeader = $requestHeader;
        return $this;
    }

    /**
     *
     */
    public function dispatch()
    {


        $this->setResponse(
            $this->post()
        );
        return $this;
    }


    /**
     * @return mixed
     * @internal param array $options
     */
    protected function post()
    {
        // POST JSON to API

        try {
            $options = [
                CURLOPT_POST => 1,
                CURLOPT_HEADER => 0,
                CURLOPT_URL => $this->getUrl(),
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_FRESH_CONNECT => 1,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_FORBID_REUSE => 1,
                CURLOPT_POSTFIELDS => http_build_query(['request'=>json_encode($this->request)]),
                CURLINFO_HEADER_OUT => 1, // capture the header info
                CURLOPT_VERBOSE => 1, // turn verbose on
                CURLOPT_RETURNTRANSFER=> true
            ];
           // r($this->request);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1:8888');
            curl_setopt_array($ch, $options);
            $result = curl_exec($ch);

            $this->requestHeader = curl_getinfo($ch, CURLINFO_HEADER_OUT);
            $this->responseHeader =  curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if (FALSE === $result) {
                throw new \Exception(curl_error($ch), curl_errno($ch));
            }

            curl_close($ch);
        } catch (\Exception $e) {
            // Handle
        }
        // Return our CURL response
        return $result;
    }

}

