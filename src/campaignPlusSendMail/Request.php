<?php
/**
 * Created by PhpStorm.
 * User: Kevinm
 * Date: 29/03/2016
 * Time: 10:40
 */
namespace campaignPlusSendMail;
class Request
{
    public $request;
    public $details;

    /**
     * @return mixed
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @param mixed $details
     * @return Request
     */
    public function setDetails($details)
    {
        $this->details = $details;
        return $this;
    }

    /**
     * @return Request
     */
    public static function RequestFactory( )
    {
        return new \campaignPlusSendMail\Request();
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     * @return Request
     */
    public function setRequest(\campaignPlusSendMail\sub\Request $request)
    {
        $this->request = $request;
        return $this;
    }


}