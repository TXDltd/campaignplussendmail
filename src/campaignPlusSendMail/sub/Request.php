<?php
/**
 * Created by PhpStorm.
 * User: Kevinm
 * Date: 29/03/2016
 * Time: 10:40
 */
namespace campaignPlusSendMail\sub;
class Request
{
    public $method;
    public $username;
    public $password;

    public static function RequestFactory( )
    {
        return new \campaignPlusSendMail\sub\Request();
    }
    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     * @return Request
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return Request
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return Request
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

}