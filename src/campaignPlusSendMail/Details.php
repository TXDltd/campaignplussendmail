<?php
/**
 * Created by PhpStorm.
 * User: Kevinm
 * Date: 29/03/2016
 * Time: 10:52
 */

namespace campaignPlusSendMail;


class Details
{
    public $commType;
    public $key;
    public $recipients;


    /**
     * @return Details
     */
    public static function DetailsFactory()
    {
        return new \campaignPlusSendMail\Details();
    }

    /**
     * @return mixed
     */
    public function getCommType()
    {
        return $this->commType;
    }

    /**
     * @param mixed $commType
     * @return Details
     */
    public function setCommType($commType)
    {
        $this->commType = $commType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     * @return Details
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @param mixed $recipients
     * @return Details
     */
    public function setRecipients(array $recipients)
    {
        $this->recipients = $recipients;
        return $this;
    }

    /**
     * @param Recipient $recipient
     * @return Details
     * @internal param mixed $recipients
     */
    public function setRecipient(\campaignPlusSendMail\Recipient $recipient)
    {
        $this->recipients[]= $recipient;
        return $this;
    }

}