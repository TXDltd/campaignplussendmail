<?php
/**
 * Created by PhpStorm.
 * User: Kevinm
 * Date: 18/03/2016
 * Time: 11:09
 */

namespace campaignPlusSendMail;
class User
{
    private $key;
    private $name;
    private $pass;


    public function __construct()
    {
    }


    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     * @return $this
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;

    }

    /**
     * @param mixed $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;

    }

    /**
     * @param mixed $pass
     * @return $this
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
        return $this;

    }


}