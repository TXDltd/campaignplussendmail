<?php
/**
 * Created by PhpStorm.
 * User: Kevinm
 * Date: 18/03/2016
 * Time: 11:16
 */

namespace campaignPlusSendMail;


class Recipient
{
    public $recipient;
    public $fname;
    public $dyn1;
    public $dyn2;
    public $dyn3;
    public $dyn4;
    public $dyn5;
    public $dyn6;
    public $dyn7;
    public $dyn8;
    public $dyn9;
    public $dyn10;

    /**
     * Recipient constructor.
     */
    public function __construct()
    {
    }
    public static function RecipientFactory(array $recipients = null)
    {
        if(empty($recipients)){
            return new \campaignPlusSendMail\Recipient();
        }
        $return = [];
            foreach($recipients as $recipient){
                $return[]=   Recipient::RecipientFactory()
                    ->setRecipient($recipient['recipient'])
                    ->setFname($recipient['fname'])
                    ->setDyn1($recipient['dyn1'])
                    ->setDyn2($recipient['dyn2'])
                    ->setDyn3($recipient['dyn3'])
                    ->setDyn4($recipient['dyn4'])
                    ->setDyn5($recipient['dyn5'])
                    ->setDyn6($recipient['dyn6'])
                    ->setDyn7($recipient['dyn7'])
                    ->setDyn8($recipient['dyn8'])
                    ->setDyn9($recipient['dyn9'])
                    ->setDyn10($recipient['dyn10']);
            }
        return $return;

    }

    /**
     * @return mixed
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param mixed $recipient
     * @return Recipient
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * @param mixed $fname
     * @return Recipient
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDyn1()
    {
        return $this->dyn1;
    }

    /**
     * @param mixed $dyn1
     * @return Recipient
     */
    public function setDyn1($dyn1)
    {
        $this->dyn1 = $dyn1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDyn2()
    {
        return $this->dyn2;
    }

    /**
     * @param mixed $dyn2
     * @return Recipient
     */
    public function setDyn2($dyn2)
    {
        $this->dyn2 = $dyn2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDyn3()
    {
        return $this->dyn3;
    }

    /**
     * @param mixed $dyn3
     * @return Recipient
     */
    public function setDyn3($dyn3)
    {
        $this->dyn3 = $dyn3;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDyn4()
    {
        return $this->dyn4;
    }

    /**
     * @param mixed $dyn4
     * @return Recipient
     */
    public function setDyn4($dyn4)
    {
        $this->dyn4 = $dyn4;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDyn5()
    {
        return $this->dyn5;
    }

    /**
     * @param mixed $dyn5
     * @return Recipient
     */
    public function setDyn5($dyn5)
    {
        $this->dyn5 = $dyn5;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDyn6()
    {
        return $this->dyn6;
    }

    /**
     * @param mixed $dyn6
     * @return Recipient
     */
    public function setDyn6($dyn6)
    {
        $this->dyn6 = $dyn6;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDyn7()
    {
        return $this->dyn7;
    }

    /**
     * @param mixed $dyn7
     * @return Recipient
     */
    public function setDyn7($dyn7)
    {
        $this->dyn7 = $dyn7;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDyn8()
    {
        return $this->dyn8;
    }

    /**
     * @param mixed $dyn8
     * @return Recipient
     */
    public function setDyn8($dyn8)
    {
        $this->dyn8 = $dyn8;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDyn9()
    {
        return $this->dyn9;
    }

    /**
     * @param mixed $dyn9
     * @return Recipient
     */
    public function setDyn9($dyn9)
    {
        $this->dyn9 = $dyn9;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDyn10()
    {
        return $this->dyn10;
    }

    /**
     * @param mixed $dyn10
     * @return Recipient
     */
    public function setDyn10($dyn10)
    {
        $this->dyn10 = $dyn10;
        return $this;
    }


}